#include <stm32l476xx.h>

int main(){

    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE5_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE5_0;
    //GPIOA->OTYPER good by default?
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_0;
    //GPIOA->PUPDR also good? For now
    GPIOA->ODR |= GPIO_ODR_ODR_5;

}