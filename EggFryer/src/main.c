#include <stm32l476xx.h>

void initClocks();
void initGPIOA();
void initTIM2();
void usDelay (uint32_t delay);
void initDHT11();
void checkResponseDHT11();
void readDHT11();
uint8_t data[5] = {};
short response = -1;
float temp;
float hum;

int main(){
    initClocks();
    initGPIOA();
    initTIM2();
    while (1){
        initDHT11();
        checkResponseDHT11();
        readDHT11();
        GPIOA->ODR |= GPIO_ODR_OD5;
        temp = (float)data[2];
        hum = (float)data[0];
        usDelay(2000000);
    }
}


void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
}

void initGPIOA(){
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE5_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE5_0;
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_0;
}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = TIM_ARR_ARR;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void usDelay (uint32_t delay){
    TIM2->CNT = 0;
    while (TIM2->CNT < delay*4);
}

void initDHT11 (){
    response = -1;
    GPIOA->MODER &= ~GPIO_MODER_MODE0_Msk;
    GPIOA->MODER |= GPIO_MODER_MODE0_0;
    GPIOA->ODR &= ~GPIO_ODR_OD0;
    usDelay(18000);
    GPIOA->ODR |= GPIO_ODR_OD0;
    usDelay(30);
    GPIOA->MODER &= ~GPIO_MODER_MODE0_Msk;
    GPIOA->PUPDR |= GPIO_PUPDR_PUPD0_0;
    
}

void checkResponseDHT11(){
    usDelay(40);
        if (!(GPIOA->IDR & GPIO_IDR_ID0)){
            usDelay(80);
            if(GPIOA->IDR & GPIO_IDR_ID0) response = 1;
            else response = 0;
        }
    while(GPIOA->IDR & GPIO_IDR_ID0);
}

void readDHT11(){

    for (int i = 0; i < 5; i++){
        for (int j = 0; j < 8; j++){
            while(!(GPIOA->IDR & GPIO_IDR_ID0));
            usDelay(40);
            if(GPIOA->IDR & GPIO_IDR_ID0) data[i] |= (1 << (7-j));
            else data[i] &= ~(1 << (7-j));
            while(GPIOA->IDR & GPIO_IDR_ID0);
        }
    }
}