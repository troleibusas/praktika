#include <stm32l476xx.h>
#include <stdio.h>

int VERY_BIG_NUMBER = 3;

void initClocks();
void initGPIO();
void initTIM2();
void initUSART2();

void USART2_IRQHandler();
void sendChar(char c);
void sendString(char *string);

int main(){

    volatile double volts = 0.;
    initClocks();
    initGPIO();
    initTIM2();
    initUSART2();
    
    ADC123_COMMON->CCR |= ADC_CCR_PRESC_3 | ADC_CCR_CKMODE_0;
    ADC1->CR &= ~ADC_CR_DEEPPWD;
    ADC1->CFGR |= ADC_CFGR_CONT;
    ADC1->SQR1 |= (5 << ADC_SQR1_SQ1_Pos);
    ADC1->CR |= ADC_CR_ADVREGEN;
    while (VERY_BIG_NUMBER--);
    ADC1->CR &= ~ADC_CR_ADCALDIF;
    ADC1->CR |= ADC_CR_ADCAL;
    while (ADC1->CR & ADC_CR_ADCAL);
    ADC1->CR |= ADC_CR_ADEN;
    while (!(ADC1->ISR & ADC_ISR_ADRDY));
    ADC1->CR |= ADC_CR_ADSTART;

    while(1){
        if (ADC1->ISR & ADC_ISR_EOC){
            volts = ADC1->DR/4096. * 3.27;
            ADC1->ISR |= ADC_ISR_EOSMP | ADC_ISR_EOC | ADC_ISR_EOS | ADC_ISR_OVR;
        }
    }

}

void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_ADCEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN | RCC_APB1ENR1_USART2EN;
}

void initGPIO(){
    GPIOA->MODER &= ~GPIO_MODER_MODE2 & ~GPIO_MODER_MODE3;
    GPIOA->MODER |= GPIO_MODER_MODE2_1 | GPIO_MODER_MODE3_1;
    GPIOA->AFR[0] |= ( 7 << GPIO_AFRL_AFSEL2_Pos) | ( 7 << GPIO_AFRL_AFSEL3_Pos);
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR3_1;
    GPIOA->PUPDR |= GPIO_PUPDR_PUPD3_0;
    GPIOA->ASCR |= GPIO_ASCR_ASC0;
}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = 1000000;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void initUSART2(){
    USART2->BRR |= (0x1B << 4) | (0x1 << 0);
    USART2->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
}

void sendChar(char c){
    if (USART2->ISR & USART_ISR_TXE) {
        USART2->TDR = c;
        while(!(USART2->ISR & USART_ISR_TC));
    }
}

void sendString(char *string){
    while (*string) {
        sendChar(*string++);
    }
}

