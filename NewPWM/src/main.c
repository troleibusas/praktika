#include <stm32l476xx.h>
#include <stdio.h>
#define BASE_SPEED 10000

void initClocks();
void initGPIO();
void initTIM2();
void initUSART2();
void initEXTI();
void EXTI15_10_IRQHandler();
void sendChar(char c);
void sendString(char *string);
int speed = 1;

int main(){

    initClocks();
    initGPIO();
    initTIM2();
    initUSART2();
    initEXTI();

    NVIC_EnableIRQ(EXTI15_10_IRQn);


}

void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN | RCC_APB1ENR1_USART2EN;
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
}

void initGPIO(){
    GPIOA->MODER &= ~GPIO_MODER_MODE0 & ~GPIO_MODER_MODE2 & ~GPIO_MODER_MODE3 & ~GPIO_MODER_MODE12;
    GPIOA->MODER |= GPIO_MODER_MODE0_1 | GPIO_MODER_MODE2_1 | GPIO_MODER_MODE3_1;
    GPIOA->AFR[0] |= ( 1 << GPIO_AFRL_AFSEL0_Pos) | ( 7 << GPIO_AFRL_AFSEL2_Pos) | ( 7 << GPIO_AFRL_AFSEL3_Pos);
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR0_1 | GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR3_1;
    GPIOA->PUPDR |= GPIO_PUPDR_PUPD3_0 | GPIO_PUPDR_PUPD12_1;

}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = BASE_SPEED;
    TIM2->CCR1 = 1000;
    TIM2->CCMR1 |= (6 << TIM_CCMR1_OC1M_Pos);
    TIM2->CCER |= TIM_CCER_CC1E;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void initUSART2(){
    USART2->BRR |= (0x1B << 4) | (0x1 << 0);
    USART2->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
}

void initEXTI(){

    SYSCFG->EXTICR[3] &= ~(0x0 << SYSCFG_EXTICR4_EXTI12_Pos);
    EXTI->IMR1 |= EXTI_IMR1_IM12;
    EXTI->RTSR1 &= ~(EXTI_RTSR1_RT12);
    EXTI->FTSR1 |= EXTI_FTSR1_FT12;
}

void EXTI15_10_IRQHandler(){
    speed++;
    if (speed > 10){
        speed = 1;
    }
    TIM2->CCR1 = BASE_SPEED * speed / 10;
    if (speed == 10) { sendString("10\n"); }
    else { sendChar((char)(48+speed)); sendChar('\n'); }
    EXTI->PR1 |= EXTI_PR1_PIF12;
}

void sendChar(char c){
    if (USART2->ISR & USART_ISR_TXE) {
        USART2->TDR = c;
        while(!(USART2->ISR & USART_ISR_TC));
    }
}

void sendString(char *string){
    while (*string) {
        sendChar(*string++);
    }
}

