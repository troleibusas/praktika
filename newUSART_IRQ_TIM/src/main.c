#include <stm32l476xx.h>


void initClocks();
void initGPIOA();
void initTIM2();
void initUSART2();
void TIM2_IRQHandler();
void USART2_IRQHandler();
void sendChar(char c);
char speed;

int main(){

    initClocks();
    initGPIOA();
    initTIM2();
    initUSART2();

    NVIC_EnableIRQ(TIM2_IRQn);
    NVIC_EnableIRQ(USART2_IRQn);


}

void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN | RCC_APB1ENR1_USART2EN;
}

void initGPIOA(){
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE5_Pos) & ~(0x3 << GPIO_MODER_MODE2_Pos) & ~(0x3 << GPIO_MODER_MODE3_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE5_0 | GPIO_MODER_MODE2_1 | GPIO_MODER_MODE3_1;
    GPIOA->AFR[0] |= ( 7 << GPIO_AFRL_AFSEL2_Pos) | ( 7 << GPIO_AFRL_AFSEL3_Pos);
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_0 | GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR3_1;
    GPIOA->PUPDR |= GPIO_PUPDR_PUPD3_0;
}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = 1000;
    TIM2->DIER |= TIM_DIER_UIE;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void initUSART2(){
    USART2->BRR |= (0x1B << 4) | (0x1 << 0);
    USART2->CR1 |= USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
}

void TIM2_IRQHandler(){
    if(TIM2->SR & TIM_SR_UIF){
        GPIOA->ODR ^= GPIO_ODR_ODR_5;
        TIM2->SR &= ~TIM_SR_UIF;
    }
    switch (speed) {
        case 'L':
            TIM2->ARR = 1000000;
            break;
        case 'M':
            TIM2->ARR = 100000;
            break;
        case 'H':
            TIM2->ARR = 50000;
            break;
        default:
            TIM2->ARR = 1000;
            break;
    }
}

void USART2_IRQHandler(){
    if (USART2->ISR & USART_ISR_RXNE){
        speed = USART2->RDR;
    }
}

void sendChar(char c){
    if (USART2->ISR & USART_ISR_TXE) {
        USART2->TDR = c;
        while(!(USART2->ISR & USART_ISR_TC));
    }
}
