#include <stm32l476xx.h>

void initClocks();
void initGPIOA();
void initTIM2();
void TIM2_IRQHandler();
void USART2_IRQHandler();
void sendChar(char c);
void sendString(char *string);
char buffer[30];
int i = 0;

int main(){

    initClocks();
    initGPIOA();
    initTIM2();

    USART2->BRR |= (0x1B << 4) | (0x1 << 0);
    USART2->CR1 |= USART_CR1_TXEIE | USART_CR1_TCIE | USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;

    NVIC_EnableIRQ(TIM2_IRQn);
    NVIC_EnableIRQ(USART2_IRQn);

}

void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN | RCC_APB1ENR1_USART2EN;
}

void initGPIOA(){
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE2_Pos) & ~(0x3 << GPIO_MODER_MODE3_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE2_1 | GPIO_MODER_MODE3_1;
    GPIOA->AFR[0] |= ( 7 << GPIO_AFRL_AFSEL2_Pos) | ( 7 << GPIO_AFRL_AFSEL3_Pos);
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2_1 | GPIO_OSPEEDER_OSPEEDR3_1;
    GPIOA->PUPDR |= GPIO_PUPDR_PUPD3_0;
}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = 1000000;
    TIM2->DIER |= TIM_DIER_UIE;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void TIM2_IRQHandler(){
    if(TIM2->SR & TIM_SR_UIF){
        TIM2->SR &= ~TIM_SR_UIF;
    }
}

void USART2_IRQHandler(){
	if (USART2->ISR & USART_ISR_RXNE){
        buffer[i] = USART2->RDR;
        i++;
        while(!TIM2->SR & TIM_SR_UIF);
    }
    if (USART2->ISR & USART_ISR_TXE){
        if (buffer[i-2] == '\r' && buffer[i-1] == '\n'){
            sendString(buffer);
            while(!(USART2->ISR & USART_ISR_TC));
            for (int i = 0; i < 30; i++) buffer[i] = 0; 
            i = 0;
        }
    }
    if (USART2->ISR & USART_ISR_TC){
        //Transmission complete
    }
}

void sendChar(char c){
    if (USART2->ISR & USART_ISR_TXE) {
        USART2->TDR = c;
        while(!(USART2->ISR & USART_ISR_TC));
    }
}

void sendString(char *string){
    while (*string) {
        sendChar(*string++);
    }
}