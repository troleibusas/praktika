#include <stm32l476xx.h>

void initGPIOA();

int main(){

    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;

    initGPIOA();

    TIM2->PSC = 4;
    TIM2->ARR = 1000000; 
    TIM2->CR1 |= TIM_CR1_CEN;

    while (1) {
        if(TIM2->SR & TIM_SR_UIF){
            GPIOA->ODR ^= GPIO_ODR_ODR_5;
            TIM2->SR &= ~TIM_SR_UIF;
        }
    }
}


void initGPIOA(){
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE5_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE5_0;
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_0;
}