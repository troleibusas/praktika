#include <stm32l476xx.h>

void initClocks();
void initGPIOA();
void initTIM2();
void TIM2_IRQHandler();

int main(){

    initClocks();
    initGPIOA();
    initTIM2();

    NVIC_EnableIRQ(TIM2_IRQn);

}

void initClocks(){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
}

void initGPIOA(){
    GPIOA->MODER &= ~(0x3 << GPIO_MODER_MODE5_Pos);
    GPIOA->MODER |= GPIO_MODER_MODE5_0;
    GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_0;
}

void initTIM2(){
    TIM2->PSC = 4;
    TIM2->ARR = 1000000;
    TIM2->DIER |= TIM_DIER_UIE;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void TIM2_IRQHandler(){
    if(TIM2->SR & TIM_SR_UIF){
        GPIOA->ODR ^= GPIO_ODR_ODR_5;
        TIM2->SR &= ~TIM_SR_UIF;
    }
}