
#include <ALB_Servo.h>
#include <DHT.h>
#include <Adafruit_LiquidCrystal.h>

#define DHT_PIN 7
#define SERVO_PIN 9
#define RELAY_FAN 13
#define RELAY_WIRE 8


DHT dht(DHT_PIN, DHT11);
Adafruit_LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
Servo servo;
bool sides; 
bool ventilationOn;
short position;
int currentTime;
int lastFanTime;
int lastDHTTime;
int lastServoTime;

void toggleVentilation();
void rotateEggs();
bool timeUp(char timer, int seconds);


void setup() {
pinMode(RELAY_FAN, OUTPUT);
pinMode(RELAY_WIRE, OUTPUT);
digitalWrite(RELAY_FAN, LOW);
digitalWrite(RELAY_WIRE, LOW);
Serial.begin(9600);
dht.begin();
lcd.begin(16, 2);
//servo.attach(SERVO_PIN);
//pinMode(SERVO_PIN, OUTPUT);
currentTime = lastFanTime = lastDHTTime = lastServoTime = millis();
sides = false;
ventilationOn = true;
}


void loop() {

  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();
  
  if (timeUp('f', 5)) {
    Serial.print("\ntoggletoggletoggle");
    toggleVentilation();
  }
  if (timeUp('d', 2)){
    lcd.setCursor(0, 0);
    lcd.print("Dregme: "); lcd.print(humidity); lcd.print("%");
    lcd.setCursor(0, 1);
    lcd.print("Temp: "); lcd.print(temperature); lcd.print("C :)");
  }
  //if ( timeUp('s', 3600) ) {
  //  rotateEggs();
  //}
}


void rotateEggs(){
  if (sides){
    for (position = 120; position >= 0; position -= 1) { 
      delay(15); 
    }
    sides = false;
  }
  else{
    for (position = 0; position <= 120; position += 1) { 
      delay(15);
    }
    sides = true;
  }
}

void toggleVentilation(){
  if (ventilationOn){
    digitalWrite(RELAY_FAN, HIGH);
    ventilationOn = false;
    Serial.print("\nVenti Off");
  }
  else{
    digitalWrite(RELAY_FAN, LOW);
    ventilationOn = true;
    Serial.print("\nVenti On");
  }
}

bool timeUp(char timer, int seconds) {
  currentTime = millis();
  if (timer == 'd'){
    if ((currentTime - lastDHTTime) > (seconds*1000L)) {
      lastDHTTime = currentTime;
      Serial.print("\nDHT: "); Serial.print(currentTime - lastDHTTime);
      return true;
    }
  }
  else if (timer == 'f'){
    if ((currentTime - lastFanTime) > (seconds*1000L)) {
      lastFanTime = currentTime;
      Serial.print("\nFan: "); Serial.print(currentTime - lastFanTime);
      return true;
    }
  }
  else if (timer == 's'){
    if ((currentTime - lastServoTime) > (seconds*1000L)) {
      lastServoTime = currentTime;
      return true;
    }
  }
  return false;
}
